variable "vpc_cidr_block" {
  type        = string
  description = "VPC Cidr block"

}

variable "subnet_cidr_block" {
  type        = string
  description = "Subnet Cidr block"
}

variable "avail_zone" {
  type        = string
  description = "Availability Zone"
}

variable "env_prefix" {
  type        = string
  description = "tags from environment"
}
variable "myip" {
  type        = string
  description = "local ip address"
}

variable "instance_type" {
  type        = string
  description = "Type of instance"
}

variable "keypair_location" {
  type = string
  description = "location of key pair"
  sensitive = true
}