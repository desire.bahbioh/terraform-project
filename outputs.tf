output "instance_id" {
  description = "instance id"
  value       = data.aws_ami.lastest-amz-image.id
}

output "instance_ip" {
  description = "instance ip address"
  value       = aws_instance.myserver.public_ip
}