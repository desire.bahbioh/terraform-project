terraform {
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 3.0"
    }
  }
}
# Configure the AWS Provider
provider "aws" {
  region  = "us-east-1"
  profile = "temp"
}

# Create a VPC
resource "aws_vpc" "my-vpc" {
  cidr_block = var.vpc_cidr_block
  tags = {
    Name = "${var.env_prefix}-vpc"
  }
}

# subnet
resource "aws_subnet" "my-subnet" {
  vpc_id            = aws_vpc.my-vpc.id
  cidr_block        = var.subnet_cidr_block
  availability_zone = var.avail_zone

  tags = {
    Name = "${var.env_prefix}-subnet"
  }
}

# internet gateway
resource "aws_internet_gateway" "my-igw" {
  vpc_id = aws_vpc.my-vpc.id

  tags = {
    Name = "${var.env_prefix}-igw"
  }
}

#route table
resource "aws_route_table" "myroute_tb" {
  vpc_id = aws_vpc.my-vpc.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.my-igw.id
  }
  tags = {
    Name = "${var.env_prefix}-rtb"
  }
}

#subnet/route table association
resource "aws_route_table_association" "a" {
  subnet_id      = aws_subnet.my-subnet.id
  route_table_id = aws_route_table.myroute_tb.id
}

#security group
resource "aws_security_group" "mysg" {
  name        = "ssh-http"
  description = "Allow ssh & http inbound traffic"
  vpc_id      = aws_vpc.my-vpc.id

  ingress {
    description = "allow ssh"
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
    cidr_blocks = [var.myip]
  }

  ingress {
    description = "allow http"
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${var.env_prefix}-sg"
  }
}

#query and get the most recent amazon AMI
data "aws_ami" "lastest-amz-image" {
  most_recent = true
  owners      = ["amazon"]
  filter {
    name   = "name"
    values = ["amzn2-*-x86_64-gp2"]
  }

  filter {
    name   = "virtualization-type"
    values = ["hvm"]
  }
}

#create a new keypair
resource "aws_key_pair" "mykeypair" {
  key_name   = "tfdbb"
  public_key = file(var.keypair_location)
}

#EC2 instance
resource "aws_instance" "myserver" {
  ami                         = data.aws_ami.lastest-amz-image.id
  instance_type               = var.instance_type
  subnet_id                   = aws_subnet.my-subnet.id
  availability_zone           = var.avail_zone
  associate_public_ip_address = true
  vpc_security_group_ids      = [aws_security_group.mysg.id]
  key_name                    = aws_key_pair.mykeypair.key_name
  tags = {
    Name = "${var.env_prefix}-server"
  }
  user_data = file("install_script.sh")
}



